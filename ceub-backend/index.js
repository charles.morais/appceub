const express = require('express');
const mysql = require('mysql2');
const bodyParser = require('body-parser');
const cors = require('cors');
const bcrypt = require('bcrypt');
require('dotenv').config();

const app = express();
const port = process.env.PORT || 3001;

app.use(cors());
app.use(bodyParser.json());

const db = mysql.createConnection({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: 'ceubapp'
});

db.connect(err => {
  if (err) {
    console.error('Error connecting to the database:', err);
    return;
  }
  console.log('Connected to the database.');
});

// Rota para registro de usuários
app.post('/register', async (req, res) => {
  const { cpf, name, birthDate, email, phone, gender, password } = req.body;
  const hashedPassword = await bcrypt.hash(password, 10);
  const query = 'INSERT INTO users (cpf, name, birth_date, email, phone, gender, password) VALUES (?, ?, ?, ?, ?, ?, ?)';
  db.query(query, [cpf, name, birthDate, email, phone, gender, hashedPassword], (err, result) => {
    if (err) {
      console.error('Error inserting user:', err);
      res.status(500).send('Error inserting user');
      return;
    }
    res.status(201).send('User registered');
  });
});

// Rota para login de usuários
app.post('/login', (req, res) => {
  const { cpf, password } = req.body;
  const query = 'SELECT * FROM users WHERE cpf = ?';
  db.query(query, [cpf], async (err, results) => {
    if (err) {
      console.error('Error fetching user:', err);
      res.status(500).send('Error fetching user');
      return;
    }
    if (results.length === 0) {
      res.status(401).send('User not found');
      return;
    }
    const user = results[0];
    const isMatch = await bcrypt.compare(password, user.password);
    if (!isMatch) {
      res.status(401).send('Invalid credentials');
      return;
    }
    res.status(200).send('Login successful');
  });
});

// Rota para obter dados do usuário
app.get('/user/:userId', (req, res) => {
  const userId = req.params.userId;
  const query = 'SELECT * FROM users WHERE id = ?';
  db.query(query, [userId], (err, results) => {
    if (err) {
      console.error('Error fetching user data:', err);
      res.status(500).send('Error fetching user data');
      return;
    }
    if (results.length === 0) {
      res.status(404).send('User not found');
      return;
    }
    const user = results[0];
    res.status(200).json(user);
  });
});

// Rota para obter consultas do usuário (clinip_appointments)
app.get('/user/:userId/clinip_appointments', (req, res) => {
  const userId = req.params.userId;
  const query = 'SELECT * FROM clinip_appointments WHERE user_id = ?';
  db.query(query, [userId], (err, results) => {
    if (err) {
      console.error('Error fetching user appointments:', err);
      res.status(500).send('Error fetching user appointments');
      return;
    }
    res.status(200).json(results);
  });
});

// Rota para agendamento de consultas
app.post('/api/appointments', (req, res) => {
  const { date, time, location } = req.body;

  const validLocations = ['Unidade CAC - Taguatinga', 'Unidade CAC - Ed. União'];
  if (!validLocations.includes(location)) {
    res.status(400).send('Localização inválida');
    return;
  }

  const hours = ['10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00'];
  if (!hours.includes(time)) {
    res.status(400).send('Horário inválido');
    return;
  }

  // Verificar se o horário já está marcado
  const checkQuery = 'SELECT * FROM clinip_appointments WHERE date = ? AND time = ? AND location = ?';
  db.query(checkQuery, [date, time, location], (checkErr, checkResults) => {
    if (checkErr) {
      console.error('Error checking appointment:', checkErr);
      res.status(500).send('Error checking appointment');
      return;
    }

    if (checkResults.length > 0) {
      res.status(400).send('Horário indisponivel');
      return;
    }

    // Se o horário não está marcado, proceder com o agendamento
    const insertQuery = 'INSERT INTO clinip_appointments (date, time, location) VALUES (?, ?, ?)';
    db.query(insertQuery, [date, time, location], (err, result) => {
      if (err) {
        if (err.code === 'ER_DUP_ENTRY') {
          res.status(400).send('Horário indisponível');
        } else {
          console.error('Error inserting appointment:', err);
          res.status(500).send('Error inserting appointment');
        }
        return;
      }
      res.status(201).json({ message: `Sua consulta foi marcada para as ${time} na ${location}`, time });
    });
  });
});

// Rota para consulta de horários disponíveis
app.get('/api/appointments/available', (req, res) => {
  const { date, location } = req.query;

  const validLocations = ['Unidade CAC - Taguatinga', 'Unidade CAC - Ed. União'];
  if (!validLocations.includes(location)) {
    res.status(400).send('Localização inválida');
    return;
  }

  const hours = ['10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00'];
  const query = 'SELECT time FROM clinip_appointments WHERE date = ? AND location = ?';

  db.query(query, [date, location], (err, results) => {
    if (err) {
      console.error('Error fetching appointments:', err);
      res.status(500).send('Error fetching appointments');
      return;
    }

    const bookedTimes = results.map(appointment => appointment.time);
    const availableTimes = hours.filter(hour => !bookedTimes.includes(hour));

    res.status(200).json(availableTimes);
  });
});

app.listen(port, () => {
  console.log(`Server running on port ${port}`);
});
