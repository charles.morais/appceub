import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Login from './components/Login';
import Register from './components/Register';
import ForgotPassword from './components/ForgotPassword';
import Dashboard from './components/Dashboard';
import Usuario from './components/Usuario';
import CliniP from './components/CliniP';
import CliniF from './components/CliniF';
import CliniL from './components/CliniL';
import CliniN from './components/CliniN';

function App() {
  return (
    <Router>
      <Routes>
        <Route path="/login" element={<Login />} />
        <Route path="/register" element={<Register />} />
        <Route path="/forgot-password" element={<ForgotPassword />} />
        <Route path="/dashboard" element={<Dashboard />} />
        <Route path="/usuario" element={<Usuario />} />
        <Route path="/CliniP" element={<CliniP />} />
        <Route path="/CliniF" element={<CliniF />} />
        <Route path="/CliniL" element={<CliniL />} />
        <Route path="/CliniN" element={<CliniN />} />
        <Route path="/" element={<Login />} />
      </Routes>
    </Router>
  );
}

export default App;
