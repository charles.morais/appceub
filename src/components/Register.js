import React, { useState } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import '../styles/Register.css';

const Register = () => {
  const [formData, setFormData] = useState({
    cpf: '',
    name: '',
    birthDate: '',
    email: '',
    phone: '',
    gender: '',
    password: '',
    confirmPassword: ''
  });

  const handleChange = (e) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (formData.password !== formData.confirmPassword) {
      alert('As senhas não coincidem');
      return;
    }
    axios.post('http://localhost:3001/register', formData)
      .then(response => {
        alert('Usuário registrado com sucesso');
        // Redirecionamento após o registro bem-sucedido (exemplo, pode ser ajustado conforme necessidade)
        // history.push('/dashboard'); // Importe useHistory se necessário
      })
      .catch(error => {
        console.error('Houve um erro ao registrar o usuário:', error);
      });
  };

  return (
    <div className="register-page">
      <div className="navbar">
        <h2>CEUB</h2>
      </div>
      <div className="content">
        <div className="left">
          <h3>Clínica CAC</h3>
          <p>
          Clínica CAC oferece atendimento comunitário acessível e de qualidade. Nossos serviços incluem fisioterapia, nutrição, análises clínicas e psicologia, todos realizados por alunos supervisionados por professores experientes. Com uma equipe dedicada e uma infraestrutura moderna, garantimos um cuidado completo e humanizado para todos os nossos pacientes.
          </p>
        </div>
        <div className="right">
          <h2>Cadastro</h2>
          <form onSubmit={handleSubmit} className="register-form">
            <label>CPF</label>
            <input type="text" name="cpf" value={formData.cpf} onChange={handleChange} />
            
            <label>Nome</label>
            <input type="text" name="name" value={formData.name} onChange={handleChange} />
            
            <label>Data de Nascimento</label>
            <input type="date" name="birthDate" value={formData.birthDate} onChange={handleChange} />
            
            <label>Email</label>
            <input type="email" name="email" value={formData.email} onChange={handleChange} />
            
            <label>Telefone</label>
            <input type="tel" name="phone" value={formData.phone} onChange={handleChange} />
            
            <div>
              <label>Sexo</label>
              <input type="radio" name="gender" value="Feminino" onChange={handleChange} /> Feminino
              <input type="radio" name="gender" value="Masculino" onChange={handleChange} /> Masculino
            </div>
            
            <label>Senha</label>
            <input type="password" name="password" value={formData.password} onChange={handleChange} />
            
            <label>Confirmação da Senha</label>
            <input type="password" name="confirmPassword" value={formData.confirmPassword} onChange={handleChange} />
            
            <button type="submit">Cadastrar</button>
          </form>
          <div className="links">
            <Link to="/login">Já tem uma conta? Faça login</Link>
            
          </div>
        </div>
      </div>
    </div>
  );
};

export default Register;
