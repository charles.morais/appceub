import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import '../styles/Usuario.css';

const UserPage = () => {
  const { userId } = useParams();

  const [userData, setUserData] = useState(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const [editMode, setEditMode] = useState(false);
  const [formData, setFormData] = useState({ name: '', email: '' });
  const [queries, setQueries] = useState([]);

  const fetchUserData = async () => {
    try {
      const response = await fetch(`/user/${userId}`);
      if (!response.ok) {
        throw new Error('Erro ao carregar dados do usuário');
      }
      const userData = await response.json();
      setUserData(userData);
      setFormData({ name: userData.name, email: userData.email });
      setLoading(false);
      setError(null); // Limpar erro em caso de sucesso
    } catch (error) {
      console.error('Erro ao buscar dados do usuário:', error);
      setError('Não foi possível carregar os dados do usuário. Verifique sua conexão ou tente novamente mais tarde.');
      setLoading(false);
    }
  };

  const fetchQueries = async () => {
    try {
      const response = await fetch(`/user/${userId}/clinip_appointments`);
      if (!response.ok) {
        throw new Error('Erro ao carregar consultas do usuário');
      }
      const queries = await response.json();
      setQueries(queries);
      setError(null); // Limpar erro em caso de sucesso
    } catch (error) {
      console.error('Erro ao buscar consultas do usuário:', error);
      setError('Não foi possível carregar as consultas do usuário. Verifique sua conexão ou tente novamente mais tarde.');
    }
  };

  useEffect(() => {
    fetchUserData();
    fetchQueries();
  }, [userId]);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const response = await fetch(`/user/${userId}`, {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(formData)
      });
      if (!response.ok) {
        throw new Error('Erro ao atualizar dados do usuário');
      }
      const result = await response.json();
      console.log(result.message);
      setEditMode(false);
      setUserData(formData);
    } catch (error) {
      console.error('Erro ao atualizar dados do usuário:', error);
      setError('Não foi possível atualizar os dados do usuário. Verifique sua conexão ou tente novamente mais tarde.');
    }
  };

  return (
    <div className="user-page">
      <div className="user-header">
        <h2>Dados do Usuário</h2>
      </div>
      <div className="user-details">
        {loading ? (
          <p>Carregando dados do usuário...</p>
        ) : error ? (
          <p className="error-message">{error}</p>
        ) : editMode ? (
          <form onSubmit={handleSubmit} className="edit-form">
            <div className="form-group">
              <label htmlFor="name">Nome:</label>
              <input type="text" id="name" name="name" value={formData.name} onChange={handleChange} />
            </div>
            <div className="form-group">
              <label htmlFor="email">Email:</label>
              <input type="email" id="email" name="email" value={formData.email} onChange={handleChange} />
            </div>
            <button type="submit">Salvar</button>
            <button type="button" onClick={() => setEditMode(false)}>Cancelar</button>
          </form>
        ) : (
          <div className="user-data">
            <p><strong>Nome:</strong> {userData.name}</p>
            <p><strong>Email:</strong> {userData.email}</p>
            <button onClick={() => setEditMode(true)}>Editar</button>
          </div>
        )}
      </div>
      <div className="user-queries">
        <h3>Consultas</h3>
        {queries.length > 0 ? (
          queries.map(query => (
            <div key={query.id} className="query-card">
              <p><strong>ID da Consulta:</strong> {query.id}</p>
              <p><strong>Data:</strong> {query.date}</p>
              <p><strong>Descrição:</strong> {query.description}</p>
            </div>
          ))
        ) : (
          <p>Não há consultas disponíveis.</p>
        )}
      </div>
    </div>
  );
};

export default UserPage;
