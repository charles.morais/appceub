import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import { FaUser } from 'react-icons/fa';
import '../styles/CliniP.css';
import planoImage from '../assets/plano.JPEG';
import taguatingaImage from '../assets/taguatinga.JPEG';

const locations = {
  taguatinga: 'Unidade CAC - Taguatinga',
  edUniao: 'Unidade CAC - Ed. União'
};

const CliniP = () => {
  const [date, setDate] = useState('');
  const [time, setTime] = useState('');
  const [location, setLocation] = useState(locations.edUniao);
  const [message, setMessage] = useState('');
  const [availableTimes, setAvailableTimes] = useState([]);
  const navigate = useNavigate();

  useEffect(() => {
    if (date) {
      fetchAvailableTimes(date);
    }
  }, [date, location]);

  const fetchAvailableTimes = async (date) => {
    try {
      const response = await axios.get(`http://localhost:3001/api/appointments/available?date=${date}&location=${location}`);
      setAvailableTimes(response.data);
    } catch (error) {
      console.error('Error fetching available times:', error);
    }
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      await axios.post('http://localhost:3001/api/appointments', {
        date,
        time,
        location
      });
      setMessage(`Sua consulta foi marcada para as ${time} do dia ${date} na ${location}`);
      fetchAvailableTimes(date);
    } catch (error) {
      setMessage(error.response.data);
    }
  };

  const handleUsuarioClick = () => {
    navigate('/usuario');
  };

  const handleLocationSelection = (selectedLocation) => {
    setLocation(selectedLocation);
    setTime('');
    if (date) {
      fetchAvailableTimes(date);
    }
  };

  return (
    <div className="clinip-container">
      <div className="ceub-navbar">
        <div className="navbar-title">
          <h2>Agendamento - Centro de Atendimento Comunitário</h2>
        </div>
        <div className="navbar-user-icon" onClick={handleUsuarioClick}>
          <FaUser size={24} />
        </div>
      </div>
      <div className="form-container">
        <h2>Escola-Clinica de Psicologia</h2>
        <h3>Marcar Consulta</h3>
        <div className="clinic-selection">
          <h4>Selecione a unidade:</h4>
          <div className="clinic-cards">
            <div
              className={`clinic-card ${location === locations.edUniao ? 'selected' : ''}`}
              onClick={() => handleLocationSelection(locations.edUniao)}
            >
              <img src={planoImage} alt="Unidade CAC - Ed. União" />
              <p>Unidade CAC - Ed. União</p>
            </div>
            <div
              className={`clinic-card ${location === locations.taguatinga ? 'selected' : ''}`}
              onClick={() => handleLocationSelection(locations.taguatinga)}
            >
              <img src={taguatingaImage} alt="Unidade CAC - Taguatinga" />
              <p>Unidade CAC - Taguatinga</p>
            </div>
          </div>
        </div>
        <form onSubmit={handleSubmit}>
          <label>
            Data:
            <input type="date" value={date} onChange={(e) => setDate(e.target.value)} required />
          </label>
          <label>
            Hora:
            <select value={time} onChange={(e) => setTime(e.target.value)} required>
              <option value="">Selecione um horário</option>
              {availableTimes.map((timeSlot) => (
                <option key={timeSlot} value={timeSlot}>{timeSlot}</option>
              ))}
            </select>
          </label>
          <button type="submit">Marcar Consulta</button>
        </form>
        {message && <p className="message-container">{message}</p>}
      </div>
    </div>
  );
};

export default CliniP;
