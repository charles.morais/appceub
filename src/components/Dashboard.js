import React from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import { FaUser } from 'react-icons/fa';
import '../styles/Dashboard.css';

import psicologiaImage from '../assets/psicologia.JPEG';
import fisioterapiaImage from '../assets/fisioterapia.JPEG';
import analisesImage from '../assets/laboratorio.JPEG';
import nutricaoImage from '../assets/nutricao.JPEG';

const Dashboard = () => {
  const location = useLocation();
  const navigate = useNavigate();
  const { user } = location.state || { user: { name: 'Usuário' } };

  const handleCardClick = (path, clinicName) => {
    navigate(path, { state: { clinic: clinicName } });
  };

  const handleUsuarioClick = () => {
    navigate('/usuario');
  };

  return (
    <div className="dashboard-container">
      <nav className="navbar">
        <div className="navbar-title">
          <h2>Agendamento - Centro de atendimento Comunitario</h2>
        </div>
        <div className="navbar-logo" onClick={handleUsuarioClick} style={{ cursor: 'pointer' }}>
          <FaUser size={24} />
        </div>
      </nav>
      
      <div className="cards-container">
        <div className="card" onClick={() => handleCardClick('/CliniP', 'Clinica-Escola de Psicologia')}>
          <img src={psicologiaImage} alt="Clinica-Escola de Psicologia" />
          <h3>Clinica-Escola de Psicologia</h3>
        </div>
        <div className="card" onClick={() => handleCardClick('/CliniF', 'Clinica-Escola de Fisioterapia')}>
          <img src={fisioterapiaImage} alt="Clinica-Escola de Fisioterapia" />
          <h3>Clinica-Escola de Fisioterapia</h3>
        </div>
        <div className="card" onClick={() => handleCardClick('/CliniL', 'Laboratorio de Analises Clinicas')}>
          <img src={analisesImage} alt="Laboratorio de Analises Clinicas" />
          <h3>Laboratorio de Analises Clinicas</h3>
        </div>
        <div className="card" onClick={() => handleCardClick('/CliniN', 'Clinica-Escola de Nutricao')}>
          <img src={nutricaoImage} alt="Clinica-Escola de Nutricao" />
          <h3>Clinica-Escola de Nutricao</h3>
        </div>
      </div>
    </div>
  );
};

export default Dashboard;
