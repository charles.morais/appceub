import React, { useState } from 'react';
import axios from 'axios';
import { Link, useNavigate } from 'react-router-dom';
import '../styles/Login.css';

const Login = () => {
  const [formData, setFormData] = useState({
    cpf: '',
    password: ''
  });
  const navigate = useNavigate();

  const handleChange = (e) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    axios.post('http://localhost:3001/login', formData)
      .then(response => {
        const user = response.data;
        alert('Login successful');
        navigate('/dashboard', { state: { user } });
      })
      .catch(error => {
        console.error('Login failed:', error);
        alert('Login failed: Invalid credentials');
      });
  };

  return (
    <div className="login-page">
      <div className="navbar">
        <h2>CEUB</h2>
      </div>
      <div className="content">
        <div className="left">
          <h3>Sobre a Clínica CAC</h3>
          <p>
          Clínica CAC oferece atendimento comunitário acessível e de qualidade. Nossos serviços incluem fisioterapia, nutrição, análises clínicas e psicologia, todos realizados por alunos supervisionados por professores experientes. Com uma equipe dedicada e uma infraestrutura moderna, garantimos um cuidado completo e humanizado para todos os nossos pacientes.
          </p>
        </div>
        <div className="right">
          <h2>Login</h2>
          <form onSubmit={handleSubmit} className="login-form">
            <label>CPF</label>
            <input type="text" name="cpf" value={formData.cpf} onChange={handleChange} required />
            
            <label>Senha</label>
            <input type="password" name="password" value={formData.password} onChange={handleChange} required />
            
            <button type="submit">Login</button>
          </form>
          <div className="links">
            <Link to="/register">Criar uma conta</Link>
            <Link to="/forgot-password">Esqueceu sua senha?</Link>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
