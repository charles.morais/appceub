import React, { useState } from 'react';
import axios from 'axios';
import '../styles/Forgotpassword.css';  // Verifique o caminho correto do arquivo CSS

const ForgotPassword = () => {
  const [formData, setFormData] = useState({
    cpf: '',
    email: ''
  });
  const [message, setMessage] = useState('');

  const handleChange = (e) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const response = await axios.post('http://localhost:3001/reset-password', formData);
      setMessage('O email de redefinição de senha foi enviado para o email cadastrado.');
      // Limpar os campos do formulário após o envio
      setFormData({
        cpf: '',
        email: ''
      });
    } catch (error) {
      console.error('Erro ao redefinir senha:', error);
      setMessage('Erro ao redefinir senha. Por favor, tente novamente.'); // Mensagem de erro genérica
    }
  };

  return (
    <div className="container">
      <h2>Esqueceu a Senha</h2>
      <form onSubmit={handleSubmit}>
        <label>Informe o seu CPF</label>
        <input
          type="text"
          name="cpf"
          value={formData.cpf}
          onChange={handleChange}
          required
        />
        
        <label>Informe o seu e-mail</label>
        <input
          type="email"
          name="email"
          value={formData.email}
          onChange={handleChange}
          required
        />
        
        <button type="submit">Enviar Senha</button>
      </form>
      {message && <p>{message}</p>}
    </div>
  );
};

export default ForgotPassword;
